import {SEARCH_EXPENSE} from "../action-types/actions";

const SearchExpense = (query) => {
    return {
        type: SEARCH_EXPENSE,
        data: query
    }
}

export default SearchExpense
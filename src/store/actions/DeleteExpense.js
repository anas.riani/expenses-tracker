import {DELETE_EXPENSE} from "../action-types/actions";

const DeleteExpense = (data) => {
    return {
        type: DELETE_EXPENSE,
        data
    }
}

export default DeleteExpense
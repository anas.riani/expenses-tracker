import {ADD_EXPENSE} from "../action-types/actions";

const AddExpense = (data) => {
    return {
        type: ADD_EXPENSE,
        data
    }
}

export default AddExpense
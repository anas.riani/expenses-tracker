import {ADD_EXPENSE, DELETE_EXPENSE, SEARCH_EXPENSE} from "../action-types/actions";

const initialiseState = () => {
    let expenses = localStorage.getItem("expenses");
    let storedExpenses = [];

    if (expenses) {
        storedExpenses = JSON.parse(expenses);
    }

    return storedExpenses;
}

let initState = {
    expanseList: initialiseState()
}

const expanseReducer = (state = initState, action) => {
    switch (action.type) {
        case ADD_EXPENSE:
            localStorage.setItem("expenses", JSON.stringify([...state.expanseList, action.data]));

            return {
                ...state,
                expanseList: [...state.expanseList, action.data]
            }
        case DELETE_EXPENSE:
            const newExpensesList = state.expanseList.filter(item => item.createdAt !== action.data.createdAt)
            localStorage.setItem("expenses", JSON.stringify(newExpensesList));

            return {
                ...state,
                expanseList: newExpensesList
            }
        case SEARCH_EXPENSE:
            localStorage.setItem("expenses", JSON.stringify(state.expanseList.filter(item => item.title.includes(action.data))));

            return {
                ...state,
                expanseList: state.expanseList.filter(item => item.title.includes(action.data))
            }
        default:
            return state;
    }
}

export default expanseReducer
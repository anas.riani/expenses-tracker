import {combineReducers} from "redux";
import expanseReducer from "./expanses";

const RootReducer = combineReducers(
    {
        expanseReducer
    }
)

export default RootReducer;
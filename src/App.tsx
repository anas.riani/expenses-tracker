import React from 'react';
import './App.css';
import Home from "./pages/home";
import Header from "./components/header";
import {Routes, Route, BrowserRouter} from "react-router-dom";
import AddExpenses from "./pages/addExpenses";
import Footer from "./components/footer";

function App() {
  return (
    <div className="App">
        <div className="container">
            <Header />
        </div>
        <BrowserRouter>
            <Routes>
                <Route path="/"  element={<Home />}/>
                <Route path="add-expenses" element={<AddExpenses />}/>
            </Routes>
        </BrowserRouter>
        <Footer />
    </div>
  );
}

export default App;

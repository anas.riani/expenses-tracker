import React from 'react';
import SearchBar from "../../components/searchBar";
import AddExpenseForm from "../../components/addExpeseForm";

const AddExpenses = () => {
    return (
        <div className="container">
            <SearchBar />
            <AddExpenseForm />
        </div>
    );
};

export default AddExpenses;
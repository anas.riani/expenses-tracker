import './home.css'
import SearchBar from "../../components/searchBar";
import ExpensesList from "../../components/expensesList";
import {ToastContainer} from "react-toastify";
import React from "react";

const Home = () => {
    return (
        <div className="container">
            <SearchBar />
            <ExpensesList />
            <ToastContainer
                position="bottom-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    )
}

export default Home;
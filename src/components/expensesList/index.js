import React from 'react';
import './expenses.css'
import {useSelector} from "react-redux";
import ElementCard from "../elementCard";

const ExpensesList = () => {

    const {expanseList: list} = useSelector(state => state.expanseReducer)

    return (
        <div className="list_container">
            {list.length ? (
                list.map(item => (
                    <ElementCard item={item}/>
                ))
            ) : (
                <div className="empty_">
                    <img src={require('../../assets/images/empty.png')} width="50%"/>
                    <p>Oops ! You didn't saved any expenses yet.</p>
                </div>
            )}
        </div>
    );
};

export default ExpensesList;
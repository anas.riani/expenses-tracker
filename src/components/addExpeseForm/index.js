import React, {useState} from 'react';
import {categories} from "../../constants/expanses_categories";
import './addExpenseForm.css';
import {useDispatch} from "react-redux";
import addExpense from "../../store/actions/addExpense";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import SuccessModal from "../common/successModal";

const AddExpenseForm = () => {
    const categoriesList = categories;
    const [categoriesShwon, setCategoriesShown] = useState(false);
    const [title, setTitle] = useState("");
    const [amount, setAmount] = useState(0);
    const [cat, setCat] = useState(null);
    const [showModal, setShowModal] = useState(false);
    const dispatch = useDispatch();

    const handleAmount = (e) => {
        let amount = e.target.value;
        if (isNaN(amount)) {
            setAmount(null)
            return
        }
        setAmount(parseFloat(amount))
    }

    const toggleDropdown = () => {
        setCategoriesShown(!categoriesShwon)
    }

    const handleCategory = (item) => {
        setCat(item);
        setCategoriesShown(false);
    }

    const AddExpense = () => {
        if (title !== "" && amount !== 0 && cat !== "Category") {
            const data = {
                title,
                amount,
                cat,
                createdAt: new Date()
            }
            dispatch(addExpense(data));
            console.log(data)
            console.log("ADD_EXPENSE Action dispatched")
            setShowModal(true);
        } else {
            // Show an error message in a toast
            const notify = () => toast.error("Thanks to validate your data !");
            notify();
            console.log("Problem encountered while data validation !")
        }
    }

    return (
        <div className="add-form">
            <ToastContainer
                position="bottom-left"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />

            <SuccessModal isOpen={showModal}/>
            <div className="form-element">
                <label htmlFor="title">Title</label>
                <input id="title" type="text" value={title} onChange={(e) => setTitle(e.target.value)}/>
            </div>
            <div className="form-element">
                <label htmlFor="amount">Amount</label>
                <input id="amount" type="number" value={amount} onChange={(e) => handleAmount(e)}/>
            </div>
            <div className="category-container">
                <div className="category-dropdown" onClick={toggleDropdown}>
                    <label htmlFor="category">
                        {
                            cat ? (
                                <div>
                                    {cat.title}
                                </div>
                            ) : (<span>Category</span>)
                        }
                    </label> &nbsp;
                    <i className="fas fa-arrow-down"/>
                </div>
                {
                    categoriesShwon&& <div className="category-dropdown-container">
                        {
                            categoriesList.map(item => (
                                <div key={item.id}
                                     id="category-item"
                                     style={{ borderRight: `1px solid ${item.color}` }}
                                     onClick={() => handleCategory(item)}>
                                     {item.title}
                                     <img src={item.icon} width="20px" style={{ color: "white" }} alt="No icon available"/>
                                </div>
                            ))
                        }
                    </div>
                }
            </div>
            <div className="add-button" onClick={AddExpense}>
                <span>Add</span> &nbsp;
                <i className="fas fa-plus"/>
            </div>
        </div>
    );
};

export default AddExpenseForm;
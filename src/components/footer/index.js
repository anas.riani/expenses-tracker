import React from 'react';
import "./footer.css";

const Footer = () => {
    return (
        <div className="footer_content">
            Made with &nbsp;<img src={require("../../assets/images/fire.png")} height="20" alt="Just a fire icon"/>&nbsp; By Anas RIANI
            <a href="https://fr.linkedin.com/in/anas-riani-027091139" id="linkedin_link" rel="noopener" target="_blank">&nbsp;(LinkedIn Profile)</a>
        </div>
    );
};

export default Footer;
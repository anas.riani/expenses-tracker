import React from 'react';
import Modal from 'react-modal';
import "./success-modal.css";
import {Link} from "react-router-dom";

const SuccessModal = ({isOpen}) => {
    const successImage = require("../../../assets/images/added-image.png");

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            backgroundColor: '#192a56',
            borderRadius: '16px'
        },
    };

    return (
        <Modal
            isOpen={isOpen}
            style={customStyles}
            contentLabel="Example Modal">
            <div className="modal-content">
                <h3>Expense added successfully</h3>
                <hr style={{ width: "100%" }} />
                <img src={successImage} alt="success image" width="200"/>
                <Link to="/" className="home-button">
                    <i className="fas fa-house-user"/> Home
                </Link>
            </div>
        </Modal>
    );
};

export default SuccessModal;
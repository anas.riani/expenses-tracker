import React from 'react';
import './element-card.css';
import moment from "moment";
import {useDispatch} from "react-redux";
import deleteExpense from "../../store/actions/DeleteExpense";
import {toast, ToastContainer} from "react-toastify";

const ElementCard = ({item}) => {
    const dispatch = useDispatch();
    const deleteItem = () =>  {
        dispatch(deleteExpense(item));
        const notify = () => toast.success("Expense deleted successfully");
        notify();
    };

    return (
        <div className="card-container" style={{ borderRight: `1px solid ${item.cat.color}` }}>
            <div className="first_part">
                <img src={item.cat.icon} height="30px"/>
                <div className="card_title">
                    <span className="little_space">{item.title}</span>
                    { moment(item.createdAt, "YYYYMMDD").fromNow() }
                </div>
            </div>
            <div className="last_part">
                    <span className="little_space">$ {item.amount}</span>
                    <i className="fas fa-trash" id="trash-icon" onClick={deleteItem}/>
            </div>
        </div>
    );
};

export default ElementCard;
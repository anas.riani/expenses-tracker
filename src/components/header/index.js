import React from 'react';
import './header.css'

const Header = () => {
    return (
        <div className="header-container">
            <div className="first_part">
                <div className="logo_container">
                    SuperXps
                    <i className="fas fa-credit-card" id="credit_card"/>
                </div>
            </div>
            <div className="second_part">
                <a href="https://github.com/nakigami" id="link_title" >
                    Stars
                    <i className="devicon-github-original" id="github"/>
                </a>
            </div>
        </div>
    );
};

export default Header;
import React, {useState} from 'react';
import './searchbar.css';
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import searchExpense from "../../store/actions/SearchExpense";

const SearchBar = () => {
    const [query, setQuery] = useState("");
    const dispatch = useDispatch();

    const handleSearch = (e) => {
        setQuery(e.target.value);
        dispatch(searchExpense(query));
    }

    return (
        <>
            {window.location.pathname === "/" ? (
                <div className="search-bar">
                    <div id="search-field">
                        <i className="fas fa-search"/>
                        <input className="search_field_input"
                               placeholder="Search ..."
                               type="text"
                               onChange={(e) => handleSearch(e)}
                        />
                    </div>
                    <Link to="/add-expenses" style={{ textDecoration: 'none' }}>
                            <a href="https://github.com/nakigami/pokemon" target="_blank" id="add_button" rel="noreferrer">
                                <i className="fas fa-plus-circle"/> &nbsp;
                                Add
                            </a>
                    </Link>
                </div>
            ) : (
                <div className="links">
                    <Link to="/" style={{ color: "#ffffff", textDecoration: 'none'}}>
                        <div className="sub_links">
                            <i className="fas fa-arrow-left"/> &nbsp;
                            <span>Back</span>
                        </div>
                    </Link>
                    <div className="sub_links">
                        <i className="far fa-window-close"/>&nbsp;
                        <span>Cancel</span>
                    </div>
                </div>
            )
            }
        </>
    );
};

export default SearchBar;